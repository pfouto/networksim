package clients;

import configuration.SimConfig;
import events.Event;
import nodes.Node;
import nodes.buckettree.BucketTreeCreator;
import nodes.buckettree.BucketTreeNode;
import nodes.nodetreeinternals.nodetree.NodeTreeInternalsCreator;
import nodes.nodetreeinternals.nodetree.NodeTreeInternalsNode;
import nodes.saturn.SaturnNode;
import nodes.saturn.SaturnNodeCreator;
import operations.Operation;
import simulator.Simulator;

import java.text.NumberFormat;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Random;

import static events.Event.EntityType.CLIENT;
import static events.Event.EntityType.NODE;
import static events.Event.EventType.PROPAGATION;

public abstract class Client {

    //bucketstate sets to false
    boolean migrateMessage = true;

    protected int id;
    private int localNode;
    private int currentNode;
    private Simulator simulator;
    private List<Node> nodes;

    public static int running = 0;

    private int currentOp;
    private int currentSequenceOp;
    private int nSequenceOps;

    private Operation lastOp = null;

    private static long localTotalOpTimes;
    private static long localNOps;
    private static long localHighestOpTime;

    private static long remoteTotalOpTimes;
    private static long remoteNOps;
    private static long remoteHighestOpTime;

    private static long migrateTotalOpTimes;
    private static long migrateNOps;
    private static long migrateHighestOpTime;

    public long throughput = -1;
    public static long time = -1;

    private long lastOpSendTime;

    private Random rand;

    public Client(int id, int initialNode, Simulator simulator, List<Node> nodes) {
        this.id = id;
        localNode = initialNode;
        currentNode = localNode;
        this.simulator = simulator;
        this.nodes = nodes;
        rand = new Random();

        currentOp = 0;
        currentSequenceOp = 0;
        nSequenceOps = genNextSequenceNumber(true);
        running++;

        localHighestOpTime = -1;
        localNOps = 0;
        localTotalOpTimes = 0;

        remoteTotalOpTimes = 0;
        remoteNOps = 0;
        remoteHighestOpTime = -1;

        migrateTotalOpTimes = 0;
        migrateNOps = 0;
        migrateHighestOpTime = -1;
    }

    public void start() {
        sendNextMessage();
    }

    public void receiveResponse(Operation op, int from, Event.EntityType fromType) {
        //System.out.println("Op " + op + " response arrived at client " + id + " from " + from);
        if (Simulator.LOG_OPS && op.id > SimConfig.nPrepOps)
            System.out.println("[" + NumberFormat.getInstance().format(simulator.getCurrentTime()) + "]\t" +
                    fromType.toString().substring(0, 1).toLowerCase() + from + " -> " + "c" + id + "\t " + op);

        assert ((int)lastOp.clientId == op.clientId && (int)op.id == lastOp.id);

        //assert !op.type.equals("read") && !op.type.equals("remote_read") || op.data != null;

        //Stats
        if(currentOp> SimConfig.nPrepOps){
            long opTime = simulator.getCurrentTime() - lastOpSendTime;
            if (op.type.equals("migrate")) { //Migrate op
                migrateTotalOpTimes += opTime;
                migrateNOps++;
                if (opTime > migrateHighestOpTime)
                    migrateHighestOpTime = opTime;
            } else if (localNode == currentNode) { //Local op
                localTotalOpTimes += opTime;
                localNOps++;
                if (opTime > localHighestOpTime)
                    localHighestOpTime = opTime;
            } else {    //Remote op
                remoteTotalOpTimes += opTime;
                remoteNOps++;
                if (opTime > remoteHighestOpTime)
                    remoteHighestOpTime = opTime;
            }
        }

        if (op.type.equals("migrate")) {
            currentNode = op.target;
            if (Simulator.LOG_NODE_CHANGE) System.out.println("Migrated to: " + currentNode);
        }

        onAfterReceiveResponse(op, from, fromType);

        if (currentOp == SimConfig.nOps + SimConfig.nPrepOps) {
            if (Simulator.LOG_NODE_CHANGE) System.out.println("TestNodeClient " + id + " finished");
            throughput = currentOp/(simulator.getCurrentTime()/1000000);
            running--;
            //if(running % 10 == 0)
                //System.out.print(running + " ");
        } else {
            sendNextMessage();
        }
    }

    abstract void onAfterReceiveResponse(Operation op, int from, Event.EntityType fromType);

    private void sendNextMessage() {
        currentOp++;
        currentSequenceOp++;

        Operation op = null;

        //change node or stay
        if (currentSequenceOp > nSequenceOps) {

            if (localNode == currentNode) {   //local
                int nextNode = chooseRandomRemoteNode();
                if (SimConfig.migrate) { //send migrate op
                    if (migrateMessage) {
                        op = generateMigrateOp(nextNode);
                        if (Simulator.LOG_NODE_CHANGE)
                            System.out.println("Migrating to node " + nextNode + ", currentOp: " + currentOp);
                    } else {
                        currentNode = nextNode;
                        if (Simulator.LOG_NODE_CHANGE)
                            System.out.println("Switching to node " + nextNode + ", currentOp: " + currentOp);
                    }
                } else { //insta switch and use remote_read/write
                    currentNode = nextNode;
                    if (Simulator.LOG_NODE_CHANGE)
                        System.out.println("Remoting to node " + currentNode + ", currentOp: " + currentOp);
                }
                currentSequenceOp = 0;
                nSequenceOps = genNextSequenceNumber(false);
            } else {  //remote
                if (SimConfig.migrate) { //send migrate op
                    if (migrateMessage) {
                        op = generateMigrateOp(localNode);

                        if (Simulator.LOG_NODE_CHANGE)
                            System.out.println("Migrating to node " + localNode + ", currentOp: " + currentOp);
                    } else {
                        currentNode = localNode;
                        if (Simulator.LOG_NODE_CHANGE)
                            System.out.println("Switching to node " + localNode + ", currentOp: " + currentOp);
                    }
                } else { //insta switch and use remote_read/write
                    currentNode = localNode;
                    if (Simulator.LOG_NODE_CHANGE)
                        System.out.println("Remoting to node " + currentNode + ", currentOp: " + currentOp);
                }
                currentSequenceOp = 0;
                nSequenceOps = genNextSequenceNumber(true);

            }
        }

        //Gen op
        if (op == null) {
            float randFloat = rand.nextFloat() * (SimConfig.readW + SimConfig.writeW);
            if (randFloat < SimConfig.readW) { //read
                if (SimConfig.migrate || currentNode == localNode) {
                    op = generateReadOp();
                } else {
                    op = generateRemoteReadOp();
                }
            } else { //write
                if (SimConfig.migrate || currentNode == localNode) {
                    op = generateWriteOp();
                } else {
                    op = generateRemoteWriteOp();
                }
            }
            if(currentOp> SimConfig.nPrepOps) {
                simulator.allOps.add(op);
            }
        }

        lastOp = op;

        onBeforeSendMessage(op);

        lastOpSendTime = simulator.getCurrentTime();
        if (SimConfig.migrate) {
            simulator.addEventToQueue(new Event(op, PROPAGATION, currentNode, NODE, id,
                    CLIENT, simulator.getCurrentTime() + getTimeToNode(currentNode)));
        } else {
            simulator.addEventToQueue(new Event(op, PROPAGATION, localNode, NODE, id,
                    CLIENT, simulator.getCurrentTime() + getTimeToNode(localNode)));
        }
    }

    private int genNextSequenceNumber(boolean local) {
        int seqN = 0;
        switch (SimConfig.distribution) {
            case "uniform":
                seqN = local ?
                        rand.nextInt(SimConfig.maxLocal - SimConfig.minLocal) + SimConfig.minLocal :
                        rand.nextInt(SimConfig.maxRemote - SimConfig.minRemote) + SimConfig.minRemote;
                break;
            case "poisson":
                seqN = getPoisson(local ? SimConfig.localPoisson : SimConfig.remotePoisson);
                break;
            default:
                System.out.println("Unknown distribution: " + SimConfig.distribution);
                System.exit(0);
        }
        if (Simulator.LOG_NODE_CHANGE)
            System.out.println("Next sequence: " + seqN + " ops");
        return seqN;
    }

    abstract void onBeforeSendMessage(Operation op);

    private long getTimeToNode(int currentNode) {
        if (currentNode == localNode) {
            return 1000;
        } else {
            return nodes.get(localNode).getConnection(currentNode) * 1000;
        }
    }

    private int chooseRandomRemoteNode() {
        int maxNode = nodes.size();

        /*Special case */
        if (nodes.get(0) instanceof SaturnNode)
            maxNode = SaturnNodeCreator.nLeafs;
        if (nodes.get(0) instanceof BucketTreeNode)
            maxNode = BucketTreeCreator.nLeafs;
        if (nodes.get(0) instanceof NodeTreeInternalsNode)
            maxNode = NodeTreeInternalsCreator.nLeafs;

        int chosenNode;
        List<Integer> currentBuckets;
        int counter = 0;
        do {
            chosenNode = rand.nextInt(maxNode);

            currentBuckets = nodes.get(chosenNode).getReplicatingBuckets(true);
            currentBuckets.removeAll(nodes.get(localNode).getReplicatingBuckets(true));
            counter++;
            assert counter < 100;
        } while (chosenNode == localNode || currentBuckets.size() == 0);
        return chosenNode;
    }

    private Operation generateReadOp() {
        Operation op = new Operation();
        op.id = currentOp;
        op.clientId = id;
        op.type = "read";
        op.bucket = randomBucket(currentNode);
        op.key = rand.nextInt(SimConfig.nKeys);
        op.node = currentNode;
        return op;
    }

    private Operation generateWriteOp() {
        Operation op = new Operation();
        op.id = currentOp;
        op.clientId = id;
        op.type = "write";
        op.bucket = randomBucket(currentNode);
        op.key = rand.nextInt(SimConfig.nKeys);
        op.data = rand.nextInt(1000);
        op.node = currentNode;
        simulator.opTimes[id][currentOp][0] = simulator.getCurrentTime();
        return op;
    }

    private Operation generateRemoteReadOp() {
        Operation op = new Operation();
        op.id = currentOp;
        op.clientId = id;
        op.type = "remote_read";
        op.bucket = randomBucket(currentNode);
        op.key = rand.nextInt(SimConfig.nKeys);
        op.node = currentNode;
        op.clientNode = localNode;
        return op;
    }

    private Operation generateRemoteWriteOp() {
        Operation op = new Operation();
        op.id = currentOp;
        op.clientId = id;
        op.type = "remote_write";
        op.bucket = randomBucket(currentNode);
        op.key = rand.nextInt(SimConfig.nKeys);
        op.data = rand.nextInt(1000);
        op.node = currentNode;
        op.clientNode = localNode;
        simulator.opTimes[id][currentOp][0] = simulator.getCurrentTime();
        return op;
    }

    /**
     * choose any bucket if localnode
     * or a bucket that the localnode does not replicate, if remote node
     */
    private int randomBucket(int currentNode) {
        if (localNode == currentNode) {
            return nodes.get(currentNode).getRandomBucket(true);
        } else {
            List<Integer> currentBuckets = nodes.get(currentNode).getReplicatingBuckets(true);
            currentBuckets.removeAll(nodes.get(localNode).getReplicatingBuckets(true));
            assert currentBuckets.size() > 0;
            return currentBuckets.get(rand.nextInt(currentBuckets.size()));
        }
    }

    private Operation generateMigrateOp(int node) {
        Operation op = new Operation();
        op.id = currentOp;
        op.clientId = id;
        op.type = "migrate";
        op.node = currentNode;
        op.target = node;
        return op;
    }

    public int getLocalNode() {
        return localNode;
    }

    private static int getPoisson(double lambda) {
        double L = Math.exp(-lambda);
        double p = 1.0;
        int k = 0;

        do {
            k++;
            p *= Math.random();
        } while (p > L);

        return k - 1;
    }

    public static  long getHighestOpTime(){
        return Math.max(localHighestOpTime, Math.max(remoteHighestOpTime, migrateHighestOpTime));
    }

    public static long getLocalHighestOpTime() {
        return localHighestOpTime;
    }

    public static long getRemoteHighestOpTime() {
        return remoteHighestOpTime;
    }

    public static long getMigrateHighestOpTime() {
        return migrateHighestOpTime;
    }

    public static long getAverageOptime() {
        return (localTotalOpTimes+remoteTotalOpTimes+migrateTotalOpTimes) / (localNOps+remoteNOps+migrateNOps);
    }

    public static long getAverageOpTimeLocal() {
        return localTotalOpTimes / localNOps;
    }

    public static long getAverageOpTimeRemote() {
        if(remoteNOps == 0) return -1;
        return remoteTotalOpTimes / remoteNOps;
    }

    public static long getAverageOpTimeMigrate() {
        if(migrateNOps == 0) return -1;
        return migrateTotalOpTimes / migrateNOps;
    }

    public static long getMigrateNOps() {
        return migrateNOps;
    }

    public static long getRemoteNOps() {
        return remoteNOps;
    }

    public static long getLocalNOps() {
        return localNOps;
    }

    public int getId() {
        return id;
    }

    public int getCurrentOp() {
        return currentOp;
    }
}
