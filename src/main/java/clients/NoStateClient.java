package clients;

import events.Event;
import nodes.Node;
import operations.Operation;
import simulator.Simulator;

import java.util.List;

public class NoStateClient extends Client{

    public NoStateClient(int id, int initialNode, Simulator simulator, List<Node> nodes) {
        super(id, initialNode, simulator, nodes);
    }

    @Override
    void onAfterReceiveResponse(Operation op, int from, Event.EntityType fromType) {

    }

    @Override
    void onBeforeSendMessage(Operation op) {

    }


}
