package clients;

import events.Event;
import nodes.Node;
import operations.Operation;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import simulator.Simulator;

import java.util.*;

public class BucketStateClient extends Client{

    // bucket, key-vers-node
    private Map<Integer, HashSet<Triple<Integer, Integer, Integer>>> context;

    public BucketStateClient(int id, int initialNode, Simulator simulator, List<Node> nodes) {
        super(id, initialNode, simulator, nodes);
        context = new HashMap<>();
        migrateMessage = false;
    }

    @Override
    void onBeforeSendMessage(Operation op) {
        Map<Integer, List<Triple<Integer, Integer, Integer>>> cont = new HashMap<>();
        for (Map.Entry<Integer, HashSet<Triple<Integer, Integer, Integer>>> e : context.entrySet()) {
            cont.put(e.getKey(), new LinkedList<>());
            for (Triple<Integer, Integer, Integer> t : e.getValue()) {
                cont.get(e.getKey()).add(new ImmutableTriple<>(t.getLeft(), t.getMiddle(), t.getRight()));
            }
        }
        op.dependencies = cont;
    }

    @Override
    void onAfterReceiveResponse(Operation op, int from, Event.EntityType fromType) {

        switch (op.type) {
            case "read":
            case "remote_read":
                if(op.response != null && op.response.getMiddle()!= null){
                    context.putIfAbsent(op.bucket, new HashSet<>());
                    context.get(op.bucket).add(new ImmutableTriple<>(op.key, op.response.getMiddle(), op.response.getRight()));
                }

                if(op.dependencies != null) {
                    for (Map.Entry<Integer, List<Triple<Integer, Integer, Integer>>> e : op.dependencies.entrySet()) {
                        context.putIfAbsent(e.getKey(), new HashSet<>());
                        for (Triple<Integer, Integer, Integer> t : e.getValue()) {
                            context.get(e.getKey()).add(new ImmutableTriple<>(t.getLeft(), t.getMiddle(), t.getRight()));
                        }
                    }
                }
                break;
            case "write":
            case "remote_write":
                context.putIfAbsent(op.bucket, new HashSet<>());
                context.get(op.bucket).add(new ImmutableTriple<>(op.key, op.version, op.node));
                break;
            case "default":
                System.out.println("Unknown message type received by BucketStateClient: " + op.type);
                System.exit(1);
        }
        trimContext();
    }

    //uglier but actually works
    private void trimContext() {
        for (HashSet<Triple<Integer, Integer, Integer>> set : context.values()) {
            if(set.isEmpty())
                continue;
            assert set.size()<10;

            int maxK = Integer.MIN_VALUE;
            int maxV = Integer.MIN_VALUE;
            int maxN = Integer.MIN_VALUE;
            int pos = -1;

            List<Triple<Integer,Integer,Integer>> l = new ArrayList<>();
            l.addAll(set);

            List<Integer> indexesToRemove = new LinkedList<>();
            for (int i = 0; i < l.size(); i++) {
                Triple<Integer, Integer, Integer> t = l.get(i);
                if (t.getMiddle() > maxV) {
                    if(pos != -1) indexesToRemove.add(pos);
                    maxK = t.getLeft();
                    maxV = t.getMiddle();
                    maxN = t.getRight();
                    pos = i;
                } else if (t.getMiddle() < maxV) {
                    indexesToRemove.add(i);
                } else if (t.getMiddle() == maxV && t.getLeft() == maxK) {
                    if (t.getRight() > maxN) {
                        if(pos != -1) indexesToRemove.add(pos);
                        maxK = t.getLeft();
                        maxV = t.getMiddle();
                        maxN = t.getRight();
                        pos = i;
                    } else {
                        indexesToRemove.add(i);
                    }
                }
            }
            for(int posToRemove : indexesToRemove) {
                set.remove(l.get(posToRemove));
            }
        }
    }
}
