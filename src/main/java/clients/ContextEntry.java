package clients;

public class ContextEntry {

    private int bucket;
    private int key;
    private int clock;
    private int node;

    public ContextEntry(int bucket, int key, int clock, int node){
        this.bucket = bucket;
        this.clock = clock;
        this.key = key;
        this.node = node;
    }

    public int getKey() {
        return key;
    }

    public int getBucket() {
        return bucket;
    }

    public int getNode() {
        return node;
    }

    public int getClock() {
        return clock;
    }

    @Override
    public String toString() {
        return bucket + ":" + key + ":" + clock + ":" + node;
    }
}
