package operations;

import configuration.SimConfig;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;

public class Operation implements Serializable{

    public Integer id = null;
    public Integer clientId = null;
    public String type = null;
    public Integer bucket = null;
    public Integer key = null;
    public Integer node = null;
    public Integer clientNode = null;
    public Integer target = null;
    public Integer data = null;
    public int[] clock = null;
    public Integer clockSingle = null;
    public Triple<Integer,Integer,Integer> response = null;
    public Integer version = null;
    public Map<Integer, List<Triple<Integer,Integer,Integer>>> dependencies = null;
    public long bufferArrivalTime = 0;

    public Operation deepClone(){
        Operation newOp = new Operation();
        newOp.id = this.id;
        newOp.clientId = this.clientId;
        newOp.type = this.type;
        newOp.bucket = this.bucket;
        newOp.key = this.key;
        newOp.node = this.node;
        newOp.clientNode = this.clientNode;
        newOp.target = this.target;
        newOp.data = this.data;
        newOp.bufferArrivalTime = this.bufferArrivalTime;
        if(this.clock != null){
            newOp.clock = new int[this.clock.length];
            System.arraycopy( this.clock, 0, newOp.clock, 0, this.clock.length );
        }
        newOp.clockSingle = this.clockSingle;
        if(this.response != null){
            newOp.response =
                    new ImmutableTriple<>(this.response.getLeft(), this.response.getMiddle(), this.response.getRight());
        }
        newOp.version = this.version;
        if(this.dependencies != null){
            newOp.dependencies = new HashMap<>();
            for(Map.Entry<Integer, List<Triple<Integer,Integer,Integer>>> e : dependencies.entrySet()){
                newOp.dependencies.put(e.getKey(), new ArrayList<>());
                for(Triple<Integer,Integer,Integer> t : e.getValue()){
                    newOp.dependencies.get(e.getKey()).add(new ImmutableTriple<>(t.getLeft(), t.getMiddle(), t.getRight()));
                }
            }
        }
        return newOp;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if(field.get(this)!= null){
                    if(field.getName().equals("clock")){
                        sb.append(field.getName()).append(":").append(Arrays.toString((int[])field.get(this))).append(" ");

                    } else {
                        sb.append(field.getName()).append(":").append(field.get(this)).append(" ");
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                System.exit(0);
            }
        }

        sb.append("] ");
        sb.append(sizeof());
        return sb.toString();
    }

    public long sizeof() {
        long size = 0;
        if (id != null) size+=4;
        if (clientId != null) size+=4;
        if (type != null) size+=4;
        if (bucket != null) size+=4;
        if (key != null) size+=4;
        if (node != null) size+=4;
        if (clientNode != null) size+=4;
        if (target != null) size+=4;
        if (data != null) size+=SimConfig.dataSize;
        if (clock != null) size+=clock.length*4;
        if (clockSingle != null) size+=4;
        if (response != null) size+=8 + SimConfig.dataSize;
        if (version != null) size+=4;
        if (dependencies != null){
            for(Map.Entry<Integer, List<Triple<Integer,Integer,Integer>>> entry: dependencies.entrySet()){
                size += 4;
                size += entry.getValue().size()*12;
            }
        }

        return size;
    }
}
