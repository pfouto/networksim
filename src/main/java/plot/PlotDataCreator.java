package plot;

import java.io.*;
import java.text.NumberFormat;
import java.util.Arrays;

public class PlotDataCreator {

    static boolean first = true;
    static String[] names;

    static boolean firstLine = true;

    public static void main(String[] args) throws IOException {


        PrintWriter timeWriter = new PrintWriter("graphs/time.dat");
        PrintWriter visibilityWriter = new PrintWriter("graphs/visibility.dat");
        PrintWriter highestWriter = new PrintWriter("graphs/highest.dat");
        PrintWriter bytesWriter = new PrintWriter("graphs/bytes.dat");
        PrintWriter clientWriter = new PrintWriter("graphs/client.dat");
        PrintWriter throughputWriter = new PrintWriter("graphs/throughput.dat");

        File[] folders = new File("results/").listFiles();
        Arrays.sort(folders);
        for (File folder : folders) {
            if (folder.isDirectory()) {

                File[] files = folder.listFiles();
                Arrays.sort(files);
                validateFiles(files);

                if(firstLine){
                    timeWriter.print("NodeType");
                    visibilityWriter.print("NodeType");
                    highestWriter.print("NodeType");
                    bytesWriter.print("NodeType");
                    clientWriter.print("NodeType");
                    throughputWriter.print("NodeType");
                    for(File file: files){
                        timeWriter.print(" " + file.getName());
                        visibilityWriter.print(" " + file.getName());
                        highestWriter.print(" " + file.getName());
                        throughputWriter.print(" " + file.getName());
                        bytesWriter.print(" " + file.getName()+ "_sent");
                        bytesWriter.print(" " + file.getName()+ "_received");
                        clientWriter.print(" " + file.getName()+ "_overall");
                        clientWriter.print(" " + file.getName()+ "_local");
                        clientWriter.print(" " + file.getName()+ "_remote");
                        clientWriter.print(" " + file.getName()+ "_migrate");
                    }
                    timeWriter.println();
                    visibilityWriter.println();
                    highestWriter.println();
                    bytesWriter.println();
                    clientWriter.println();
                    throughputWriter.println();
                    firstLine = false;
                }

                timeWriter.print(folder.getName());
                visibilityWriter.print(folder.getName());
                highestWriter.print(folder.getName());
                bytesWriter.print(folder.getName());
                clientWriter.print(folder.getName());
                throughputWriter.print(folder.getName());
                for(File file: files){
                    BufferedReader br = new BufferedReader(new FileReader(file));

                    //Time
                    String line[] = br.readLine().split(" ");
                    timeWriter.print(" " + Long.valueOf(line[line.length-1]));
                    //Throughtput
                    line = br.readLine().split(" ");
                    throughputWriter.print(" " + Long.valueOf(line[line.length-1]));

                    //Visibility Average
                    br.readLine();
                    line = br.readLine().split(" ");
                    visibilityWriter.print(" " + Long.valueOf(line[line.length-2]));
                    //Visibility Max
                    line = br.readLine().split(" ");
                    highestWriter.print(" " + Long.valueOf(line[line.length-1]));
                    //Bytes
                    String line2;
                    long sent = 0;
                    long received = 0;
                    br.readLine();
                    while(!(line2 = br.readLine()).startsWith("Average/Max")){
                        String[] line2Split = line2.split("\t");
                        sent += Long.valueOf(line2Split[1]);
                        received += Long.valueOf(line2Split[2]);
                    }
                    bytesWriter.print(" " + sent);
                    bytesWriter.print(" " + received);
                    //Client Response
                    String line3 = "";
                    while(!line3.startsWith("Average/Max Client Response Times")){
                        line3 = br.readLine();
                    }
                    //br.readLine();
                    String line3split[] = br.readLine().split("\t");
                    clientWriter.print(" " + line3split[2]);
                    line3split = br.readLine().split("\t");
                    clientWriter.print(" " + line3split[2]);
                    line3split = br.readLine().split("\t");
                    clientWriter.print(" " + line3split[2]);
                    line3split = br.readLine().split("\t");
                    clientWriter.print(" " + line3split[2]);

                }
                timeWriter.println();
                visibilityWriter.println();
                highestWriter.println();
                bytesWriter.println();
                clientWriter.println();
                throughputWriter.println();
            }
        }
        timeWriter.close();
        visibilityWriter.close();
        highestWriter.close();
        bytesWriter.close();
        clientWriter.close();
        throughputWriter.close();
    }

    static void validateFiles(File[] files){
        if(first){
            first = false;
            names = new String[files.length];
            for(int i =0;i<files.length;i++){
                names[i] = files[i].getName();
            }
        } else {
            for(int i =0;i<files.length;i++){
                if (!names[i].equals(files[i].getName())){
                    System.out.println("File names not consistent");
                    for(int j =0;j<files.length;j++) {
                        System.out.println(files[j] + " " + names[j]);
                    }
                    System.exit(1);
                }
            }
        }
    }
}