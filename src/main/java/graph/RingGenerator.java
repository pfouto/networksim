package graph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RingGenerator {

    public static void main(String[] args) throws IOException, InterruptedException {

        int[][] latencyMatrix = readLatenciesFromFile("res/nodetree/ringconfig");

        ArrayList<Integer> nodes = new ArrayList<>();
        nodes.add(0);nodes.add(1);nodes.add(2);
        nodes.add(3);nodes.add(4);nodes.add(5);

        for(int i = 0;i<latencyMatrix.length;i++){
            for(int j = 0;j<latencyMatrix[0].length;j++){
                System.out.print(latencyMatrix[i][j] + "\t");
            }
            System.out.println();
        }

        int lowestCost = Integer.MAX_VALUE;
        while(true){
            Collections.shuffle(nodes);
            List<Integer> costs = calcCost(nodes, latencyMatrix);
            int cost = 0;
            for(int c : costs) cost +=c;
            if(cost < lowestCost){
                lowestCost = cost;
                System.out.println(cost);
                System.out.println(nodes);
                System.out.println(costs);
            }
        }

    }

    private static int[][] readLatenciesFromFile(String s) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(s));

        //latency matrix
        int nNodes = Integer.valueOf(br.readLine());
        int[][] latencyMatrix = new int[nNodes][nNodes];
        for (int i = 0; i < nNodes; i++) {
            String[] costs = br.readLine().trim().split("\\s+");
            for (int j = 0; j < nNodes; j++) {
                latencyMatrix[i][j] = Integer.valueOf(costs[j]);
            }
        }
        br.close();
        return latencyMatrix;
    }

    private static List<Integer> calcCost(ArrayList<Integer> nodes, int[][] latencyMatrix) {
        List<Integer> cost = new ArrayList<>();
        for(int i=0;i<nodes.size();i++){
            cost.add(latencyMatrix[nodes.get(i)][nodes.get((i+1)%nodes.size())]);
        }
        return cost;
    }

}