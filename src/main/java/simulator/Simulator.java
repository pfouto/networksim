package simulator;

import clients.BucketStateClient;
import clients.Client;
import clients.NoStateClient;
import configuration.SimConfig;
import events.Event;
import nodes.*;
import nodes.buckettree.BucketTreeCreator;
import nodes.buckettree.BucketTreeNode;
import nodes.nodetree.NodeTreeCreator;
import nodes.nodetreeinternals.nodetree.NodeTreeInternalsCreator;
import nodes.nodetreeinternals.nodetree.NodeTreeInternalsNode;
import nodes.saturn.SaturnNode;
import nodes.saturn.SaturnNodeCreator;
import operations.Operation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class Simulator {

    public static final boolean LOG_OPS = false;
    public static final boolean LOG_NODE_CHANGE = false;
    public static final boolean LOG_ALG_DEBUG = false;
    private static final boolean LOG_FINAL_FILE = true;

    private List<Client> clients;
    private List<Node> nodes;

    private PriorityQueue<Event> eventsQueue;

    private long currentTime = 0;

    public List<Operation> allOps;

    //UserId, OpId, 0created/1executed
    public long[][][] opTimes;

    public Simulator() throws IOException {

        allOps = new ArrayList<>();

        eventsQueue = new PriorityQueue<>(Comparator.comparingLong(Event::getTime));

        //Setup nodes

        switch (SimConfig.nodeType){
            case "NodeTree":
            case "nodetree":
                nodes = NodeTreeCreator.createNodes(this, SimConfig.nodeConfigFile);
                break;
            case "NodeTreeInternals":
            case "nodetreeinternals":
                nodes = NodeTreeInternalsCreator.createNodes(this, SimConfig.nodeConfigFile);
                break;
            case "BucketTree":
            case "buckettree":
                nodes = BucketTreeCreator.createNodes(this, SimConfig.nodeConfigFile);
                break;
            case "Saturn":
            case "saturn":
                nodes = SaturnNodeCreator.createNodes(this, SimConfig.nodeConfigFile);
                break;
            default:
                System.out.println("Unknown nodeType in config: " + SimConfig.nodeType);
                System.exit(1);

        }

        //Setup clients
        clients = new ArrayList<>();

        int maxNode = nodes.size();
        /*Special saturn case */
        if (nodes.get(0) instanceof SaturnNode)
            maxNode = SaturnNodeCreator.nLeafs;
        if (nodes.get(0) instanceof BucketTreeNode)
            maxNode = BucketTreeCreator.nLeafs;
        if (nodes.get(0) instanceof NodeTreeInternalsNode)
            maxNode = NodeTreeInternalsCreator.nLeafs;

        for (int i = 0; i < SimConfig.nClients; i++) {
            Client c;
            if(nodes.get(0) instanceof BucketTreeNode)
                c = new BucketStateClient(i, i % maxNode, this, nodes);
            else
                c = new NoStateClient(i, i % maxNode, this, nodes);
            clients.add(c);
        }

        opTimes = new long[SimConfig.nClients][SimConfig.nOps+SimConfig.nPrepOps+1][3];

    }

    public void addEventToQueue(Event ev) {
        eventsQueue.add(ev);
    }

    public void startSimulation() throws FileNotFoundException {
        //System.out.println(" ------------ Sim starting -------------");
        //SimConfig.printConfig();

        clients.forEach(Client::start);
        Event lastEv = null;
        while (!eventsQueue.isEmpty()) {
            Event lastEv2 = lastEv;
            Event ev = eventsQueue.poll();
            currentTime = ev.getTime();
            Node n;
            lastEv = ev;
            switch (ev.getType()) {
                case PROPAGATION:
                    if (ev.getTargetType() == Event.EntityType.CLIENT) {
                        Client c = clients.get(ev.getTarget());
                        c.receiveResponse(ev.getOp(), ev.getFrom(), ev.getFromType());
                    } else if (ev.getTargetType() == Event.EntityType.NODE) {
                        n = nodes.get(ev.getTarget());
                        n.addToBuffer(ev.getOp(), ev.getFrom(), ev.getFromType());
                        n.startProcessNext();
                    }
                    break;
                case PROCESSING:
                    n = nodes.get(ev.getTarget());
                    n.processingFinished(ev.getOp(), ev.getFrom(), ev.getFromType());
                    n.startProcessNext();
                    break;
                case TIMER:
                    n = nodes.get(ev.getTarget());
                    n.timer();
                    break;
                default:
                    System.out.println("Unknown Operation Type: " + ev.getType().name());
                    System.exit(1);
            }
        }

        assert Client.running == 0;

        if (LOG_FINAL_FILE) {

            String fileName = "results/"+SimConfig.dataSize + "-" + (SimConfig.migrate ? "migrate" : "remote") +"/"
                    + SimConfig.configName;
            File file = new File(fileName);
            file.getParentFile().mkdirs();

            PrintWriter writer = new PrintWriter(fileName);

            writer.println("Time: " + getCurrentTime()/1000000);
            writer.println("Throughput: " + clients.stream().mapToLong(c -> c.throughput).sum());

            writer.println("Write visibility time:");
            /*calc times*/
            long total = 0;
            int n = 0;
            long totalB = 0;
            int nB = 0;
            long highest = 0;

            for (long[][] cTimes : opTimes) {
                for (long[] opTime : cTimes) {
                    if (opTime[0] == 0 && opTime[1] == 0) continue;
                    long timeTaken = opTime[1] - opTime[0];
                    n++;
                    total = Math.addExact(total, timeTaken);
                    if (timeTaken > highest)
                        highest = timeTaken;

                    totalB+=opTime[2] - opTime[0];
                    nB++;
                }
            }

            writer.println("\tAvg exec time: " + (total / n)/1000 + " " + (totalB/nB)/1000);
            writer.println("\tHighest: " + highest/1000);
            writer.println("Bytes sent/received:");
            for (Node node : nodes) {
                writer.println(node.getId() + "\t" + node.getBytesSent() + "\t" + node.getBytesReceived());
            }
            writer.println("Average/Max Buffer Size:");
            for (Node node : nodes) {
                writer.println(node.getId()
                        + "\t" + node.getAverageBufferSize()
                        + "\t" + node.getMaxBufferSize());
            }
            writer.println("Average/Max Client Response Times:");
            writer.println("\t" + "Overall"
                            + "\t" + (Client.getAverageOptime()/1000)
                            + "\t" + (Client.getHighestOpTime()/1000) + "\n"
                            + "\t" + "Local"
                            + "\t" + (Client.getAverageOpTimeLocal()/1000)
                            + "\t" + (Client.getLocalHighestOpTime()/1000)
                            + "\t" + Client.getLocalNOps()
                            + "\n"
                            + "\t" + "Remote"
                            + "\t" + (Client.getAverageOpTimeRemote()/1000)
                            + "\t" + (Client.getRemoteHighestOpTime()/1000)
                            + "\t" + Client.getRemoteNOps()
                            + "\n"
                            + "\t" + "Migrate"
                            + "\t" + (Client.getAverageOpTimeMigrate()/1000)
                            + "\t" + (Client.getMigrateHighestOpTime()/1000)
                            + "\t" + Client.getMigrateNOps()
            );
            writer.println("Average/Max Send Delay:");
            for (Node node : nodes) {
                writer.println(node.getId()
                        + "\t" + node.getAverageDelay()
                        + "\t" + node.getMaxDelay());
            }
            writer.println("Read AM/Write AM Execution Delay:");
            for (Node node : nodes) {
                if(node instanceof SaturnNode && !((SaturnNode)node).isLeaf()) continue;
                if(node instanceof BucketTreeNode && !((BucketTreeNode)node).isLeaf()) continue;
                if(node instanceof NodeTreeInternalsNode && !((NodeTreeInternalsNode)node).isLeaf()) continue;
                writer.println(node.getId()
                        + "\t" + node.getAverageReadExecutionDelay()
                        + "\t" + node.getMaxReadExecutionDelay()
                        + "\t" + node.getAverageWriteExecutionDelay()
                        + "\t" + node.getMaxWriteExecutionDelay());
            }
            writer.close();
        }
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public List<Client> getClients() {
        return clients;
    }
}