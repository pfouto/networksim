package nodes.saturn;

import configuration.SimConfig;
import events.Event;
import nodes.Node;
import operations.Operation;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import simulator.Simulator;
import org.apache.commons.lang3.tuple.Triple;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static events.Event.EntityType.NODE;

public class SaturnNode extends Node {

    private boolean leaf;
    //[bucket, [key, [value, timestamp, nodeid]]
    private Map<Integer, Map<Integer, Triple<Integer, Integer, Integer>>> keys;
    private int clock = 0;

    SaturnNode(int id, List<Node> nodes, Simulator simulator, boolean isLeaf, boolean[] buckets, int nodeSharedLink) {
        super(id, nodes, simulator, buckets, nodeSharedLink);
        this.leaf = isLeaf;
        if (leaf) {
            keys = new HashMap<>();
            for (int i = 0; i < buckets.length; i++) {
                if (buckets[i]) keys.put(i, new HashMap<>());
            }
        }
    }

    @Override
    public long startProcessing(Operation op, int from, Event.EntityType fromType) {
        switch (op.type) {
            case "write":
            case "remote_write":
                return handleWrite(op, from, fromType);
            case "read":
            case "remote_read":
                return handleRead(op, from, fromType);
            case "read_response":
                return handleReadResponse(op, from, fromType);

            case "migrate":
                return handleMigrate(op, from, fromType);
            case "data":
                //do nothing
                return BASE_PROCESS_TIME;
            default:
                System.out.println("Unknown message type received: " + op.type);
                System.exit(1);
        }
        return 0;
    }

    private long handleWrite(Operation op, int from, Event.EntityType fromType) {

        long time = BASE_PROCESS_TIME;

        Operation metaOp;

        //Propagate
        if (fromType == Event.EntityType.CLIENT) {
            metaOp = op.deepClone();
            metaOp.data = null;
            metaOp.clockSingle = ++clock;

            //send data
            Operation dataOp = op.deepClone();
            dataOp.type = "data";
            for (int node : this.connections.keySet()) {
                if (isLeaf(node) && node != id && isInterested(node, metaOp.bucket, id)) {
                    propagateOpToNode(dataOp, node);
                }
            }
        } else {
            metaOp = op.deepClone();
            clock = Math.max(clock, metaOp.clockSingle);
        }
        for (int node : this.connections.keySet()) {
            if (node == from && fromType == NODE) continue; //Dont send back
            if (!isInterested(node, metaOp.bucket, id)) continue;
            if (!isLeaf(node) || !leaf) { //feio mas funciona
                propagateOpToNode(metaOp, node);
            }
        }

        //perfom write if relevant
        if (leaf && buckets[metaOp.bucket]) {
            assert op.bufferArrivalTime != 0;
            long delay = simulator.getCurrentTime() - op.bufferArrivalTime;
            nWriteExecutionDelays++;
            totalWriteExecutionDelay += delay;
            if (delay > maxWriteExecutionDelay)
                maxWriteExecutionDelay = delay;
            if (opIsStrictNewer(metaOp.bucket, metaOp.key, metaOp.clockSingle, metaOp.node))
                keys.get(metaOp.bucket).put(metaOp.key, new ImmutableTriple<>(1, metaOp.clockSingle, metaOp.node));
            time += ((SimConfig.dataSize * 1000000) / WRITE_SPEED);

            simulator.opTimes[metaOp.clientId][metaOp.id][1] = Math.max(simulator.getCurrentTime() + time, simulator.opTimes[metaOp.clientId][metaOp.id][1]);
        }

        //return to client
        if (metaOp.node == id) {
            propagateOpToClient(metaOp, metaOp.clientId);
        }
        return time;
    }

    private long handleReadResponse(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;
        if (leaf) {
            assert op.clientNode == id;
            op.data = -1;
            propagateOpToClient(op, op.clientId);
            return time;
        } else {
            propagateOpToNode(op, nextNodeToTarget(op.clientNode));
            return time;
        }
    }

    private long handleRead(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;
        //Respond to client if directed to me
        if (op.node == id) { //reply
            assert buckets[op.bucket];
            Triple<Integer, Integer, Integer> triple = keys.get(op.bucket).get(op.key);
            assert op.bufferArrivalTime != 0;
            long delay = simulator.getCurrentTime() - op.bufferArrivalTime;
            nReadExecutionDelays++;
            totalReadExecutionDelay += delay;
            if (delay > maxReadExecutionDelay)
                maxReadExecutionDelay = delay;
            op = op.deepClone();
            if (op.type.equals("read")) {
                op.data = (triple != null ? triple.getLeft() : -1); //assume theres always data to read
                op.type = "read_response";
                propagateOpToClient(op, op.clientId);
            } else if (op.type.equals("remote_read")) {
                //remote
                Operation dataOp = op.deepClone();
                dataOp.data = (triple != null ? triple.getLeft() : -1); //assume theres always data to read
                dataOp.type = "data";
                propagateOpToNode(dataOp, op.clientNode);
                op.type = "read_response";
                propagateOpToNode(op, nextNodeToTarget(op.clientNode));
            } else {
                System.out.println("BUG");
                System.exit(1);
            }
            return ((SimConfig.dataSize * 1000000) / READ_SPEED) + time;
        } else { //propagate forward
            assert op.type.equals("remote_read");
            propagateOpToNode(op, nextNodeToTarget(op.node));
            return time;
        }
    }

    private long handleMigrate(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;
        if (op.target == id) { //Check if I'm target
            assert leaf;
            propagateOpToClient(op, op.clientId);
        } else {
            propagateOpToNode(op, nextNodeToTarget(op.target));
        }
        return time;
    }

    private boolean opIsStrictNewer(int bucket, int key, int opClock, int originalNode) {
        Triple<Integer, Integer, Integer> triple = keys.get(bucket).get(key);
        return triple == null || triple.getMiddle() < opClock || triple.getMiddle() <= opClock &&
                triple.getRight() < originalNode;
    }

    private int nextNodeToTarget(int target) {
        for (int node : this.connections.keySet()) {
            if (leaf && isLeaf(node)) continue;
            if (leadsTo(node, target, id)) {
                return node;
            }
        }
        assert false;
        System.out.println("No path to " + target);
        System.exit(1);
        return -1;
    }

    private boolean leadsTo(int node, int targetNode, int from) {
        SaturnNode n = (SaturnNode) nodes.get(node);
        if (node == targetNode) {
            return true;
        } else {
            for (Map.Entry<Integer, Integer> entry : n.getConnections().entrySet()) {
                if (isLeaf(node) && isLeaf(entry.getKey())) continue;
                if (entry.getKey() != from && leadsTo(entry.getKey(), targetNode, node))
                    return true;
            }
        }
        return false;
    }

    private boolean isInterested(int node, int bucket, int from) {
        SaturnNode n = (SaturnNode) nodes.get(node);
        if (n.isLeaf()) {
            return n.getBucketsList()[bucket];
        } else {
            for (Map.Entry<Integer, Integer> entry : n.getConnections().entrySet()) {
                if (entry.getKey() != from && isInterested(entry.getKey(), bucket, node))
                    return true;
            }
        }
        return false;
    }

    private boolean isLeaf(int nodeId) {
        return ((SaturnNode) nodes.get(nodeId)).isLeaf();
    }

    public boolean isLeaf() {
        return leaf;
    }

}
