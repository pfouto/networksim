package nodes.saturn;

import nodes.Node;
import simulator.Simulator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SaturnNodeCreator {

    public static int nLeafs;
    private static int[][] matrix;

    private static boolean[][] buckets;

    private static int[] nodeSharedLink;

    public static List<Node> createNodes(Simulator sim, String configFile) throws IOException {
        buildNodesMatrix(configFile);
        return buildNodesFromMatrix(sim);
    }

    private static void buildNodesMatrix(String configFile) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(configFile));
        int nNodes = Integer.valueOf(br.readLine());
        nLeafs = Integer.valueOf(br.readLine());
        matrix = new int[nNodes][nNodes];
        nodeSharedLink = new int[nNodes];
        for(int i = 0;i< nNodes;i++){
            nodeSharedLink[i] = i;
        }

        for(int i = 0;i<nNodes;i++){
            String line = br.readLine();
            String[] costs = line.trim().split("\\s+");
            for(int j = 0;j<nNodes;j++){
                matrix[i][j] = Integer.valueOf(costs[j]);
            }
            if(costs.length > nNodes){
                nodeSharedLink[i] = Integer.valueOf(costs[nNodes]);
            }
        }
        br.readLine();

        int nBuckets = Integer.valueOf(br.readLine());
        buckets = new boolean[nLeafs][nBuckets];
        for(int i = 0;i<nLeafs;i++){
            String line = br.readLine();
            String[] value = line.trim().split("\\s+");
            for (String aValue : value) {
                buckets[i][Integer.valueOf(aValue)] = true;
            }
        }
    }

    private static List<Node> buildNodesFromMatrix(Simulator sim) {
        List<Node> nodes = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            Node newNode;
            if(i < nLeafs){
                newNode = new SaturnNode(i, nodes, sim, true, buckets[i], nodeSharedLink[i]);
            } else {
                newNode = new SaturnNode(i, nodes, sim, false, null, nodeSharedLink[i]);
            }
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] != -1) {
                    newNode.addConnection(j, matrix[i][j]);
                }
            }
            nodes.add(newNode);
        }
        return nodes;
    }
}