package nodes;

import configuration.SimConfig;
import events.Event;
import operations.Operation;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import simulator.Simulator;
import org.apache.commons.lang3.tuple.Triple;

import java.text.NumberFormat;
import java.util.*;

import static events.Event.EntityType.CLIENT;
import static events.Event.EntityType.NODE;
import static events.Event.EventType.PROCESSING;
import static events.Event.EventType.PROPAGATION;
import static events.Event.EventType.TIMER;

public abstract class Node {

    private static final long LINK_SPEED = Long.MAX_VALUE; //125000000;//bytes per second  //1GBps
    protected static final long WRITE_SPEED = Long.MAX_VALUE; //1100000; //bytes per second  //1.1MBps
    protected static final long READ_SPEED = Long.MAX_VALUE; //600000;   //bytes per second  //0.6MBps
    protected static final int BASE_PROCESS_TIME = 10;

    protected int id;
    //Node, time
    protected Map<Integer, Integer> connections;
    protected List<Node> nodes;
    //Op, from, fromtype
    private Queue<Triple<Operation, Integer, Event.EntityType>> operationsBuffer;
    protected boolean[] buckets;
    private boolean processing;
    protected Simulator simulator;
    private long nextMessage;

    private List<Triple<Operation,Integer, Event.EntityType>> propQueue;

    private int totalBufferSize;
    private int maxBufferSize;
    private int nBufferSizes;

    private long totalDelay;
    private long maxDelay;
    private long nDelays;

    private long bytesSent;
    private long bytesReceived;

    protected long totalWriteExecutionDelay;
    protected long totalReadExecutionDelay;
    protected long maxReadExecutionDelay;
    protected long maxWriteExecutionDelay;
    protected long nWriteExecutionDelays;
    protected long nReadExecutionDelays;

    private int nodeSharedLink;

    public Node(int id, List<Node> nodes, Simulator simulator, boolean[] buckets, int nodeSharedLink){
        this.id = id;
        this.nodes = nodes;
        connections = new HashMap<>();
        operationsBuffer = new LinkedList<>();
        processing = false;
        this.simulator = simulator;
        this.buckets = buckets;
        nextMessage = 0;
        propQueue = new LinkedList<>();
        bytesSent = 0;
        bytesReceived = 0;
        totalBufferSize = 0;
        nBufferSizes = 0;
        maxBufferSize = 0;
        totalDelay = 0;
        maxDelay = 0;
        nDelays = 0;
        maxReadExecutionDelay = 0;
        maxWriteExecutionDelay = 0;
        this.nodeSharedLink = nodeSharedLink;
    }

    public Node(int id, List<Node> nodes, Simulator simulator, boolean[] buckets) {
        this(id, nodes, simulator, buckets, id);
    }

    protected void propagateOpToClient(Operation op, int to) {

        propQueue.add(new ImmutableTriple<>(op.deepClone(), to, Event.EntityType.CLIENT));
    }

    protected void propagateOpToNode(Operation op, int to) {
        propQueue.add(new ImmutableTriple<>(op.deepClone(), to, Event.EntityType.NODE));
    }

    protected void startTimer(long time) {
        long currentTime = simulator.getCurrentTime();
        simulator.addEventToQueue(new Event(null, TIMER, id, NODE, id, NODE, currentTime + time));
    }

    public void addToBuffer(Operation op, int from, Event.EntityType fromType) {
        if (Simulator.LOG_OPS && op.id > SimConfig.nPrepOps)
            System.out.println("[" + NumberFormat.getInstance().format(simulator.getCurrentTime()) + "]\t" +
                    fromType.toString().substring(0, 1).toLowerCase() + from + " -> " + "n" + id + "\t " + op);

        long operationBytes = op.sizeof();
        if (op.id >= SimConfig.nPrepOps) {
            bytesReceived = Math.addExact(bytesReceived, operationBytes);
        }
        operationsBuffer.offer(new ImmutableTriple<>(op, from, fromType));
        op.bufferArrivalTime = simulator.getCurrentTime();
        if (op.id > SimConfig.nPrepOps) {
            totalBufferSize += operationsBuffer.size();
            nBufferSizes++;
            if (operationsBuffer.size() > maxBufferSize) {
                maxBufferSize = operationsBuffer.size();
            }
        }
        if(op.type.equals("write") || op.type.equals("remote_write")){
            simulator.opTimes[op.clientId][op.id][2] = simulator.getCurrentTime();
        }
    }

    public void startProcessNext() {
        if (!processing && !operationsBuffer.isEmpty()) {
            processing = true;
            Triple<Operation, Integer, Event.EntityType> entry = operationsBuffer.poll();
            Operation op = entry.getLeft();

            //TODO
            //op.bufferArrivalTime = simulator.getCurrentTime();
            int from = entry.getMiddle();
            Event.EntityType fromType = entry.getRight();
            //System.out.println(
            // "Op " + op + " from " +  from +" started being proccess by " + id + " in time " + currentTime);
            long procTime = startProcessing(op, from, fromType);
            assert (procTime > 0);
            long time = simulator.getCurrentTime() + procTime;
            simulator.addEventToQueue(new Event(op, PROCESSING, id, NODE, from, fromType, time));
        }
    }

    //Return the time it takes to process operation op
    public abstract long startProcessing(Operation op, int from, Event.EntityType fromType);

    private long getTimeToClient(int client) {

        int clientNode = simulator.getClients().get(client).getLocalNode();
        if (clientNode == id) {
            return 5*1000;
        } else {
            return getConnection(clientNode) * 1000;
        }
    }


    public void processingFinished(Operation op, int from, Event.EntityType fromType) {
        processing = false;

        Iterator<Triple<Operation, Integer, Event.EntityType>> it = propQueue.iterator();

        while(it.hasNext()){
            Triple<Operation, Integer, Event.EntityType> triple = it.next();
            Operation sendOp = triple.getLeft();
            int destId = triple.getMiddle();
            Event.EntityType destType = triple.getRight();

            long operationBytes = sendOp.sizeof();
            long timeToSend = ((operationBytes * 1000000L)/ LINK_SPEED );

            long currentTime = simulator.getCurrentTime();
            long delay = Math.max(0, nodes.get(nodeSharedLink).nextMessage - currentTime);
            long sendTime = currentTime + delay + timeToSend;

            if(destType == CLIENT)
                simulator.addEventToQueue(new Event(sendOp, PROPAGATION, destId,
                        CLIENT, id, NODE, sendTime + getTimeToClient(destId)));
            else if (destType == NODE)
                simulator.addEventToQueue(new Event(sendOp, PROPAGATION, destId,
                        NODE, id, NODE, sendTime + (connections.get(destId) * 1000)));
            else
                assert false;
            nodes.get(nodeSharedLink).nextMessage = sendTime;

            if (sendOp.id > SimConfig.nPrepOps) {
                bytesSent = Math.addExact(bytesSent, operationBytes);
            }

            if (op.id > SimConfig.nPrepOps) {
                totalDelay += delay;
                nDelays++;
                if (delay > maxDelay)
                    maxDelay = delay;
            }
            it.remove();
        }
    }

    public int getId() {
        return id;
    }

    public Map<Integer, Integer> getConnections() {
        return connections;
    }

    public void addConnection(Integer targetNode, int distance) {
        connections.put(targetNode, distance);
    }

    public Integer getConnection(Integer targetNode) {
        return connections.get(targetNode);
    }

    public boolean isProcessing() {
        return processing;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder().append("Node: ").append(id).append(" -> [");
        for (Map.Entry<Integer, Integer> e : connections.entrySet()) {
            sb.append(e.getKey()).append(": ").append(e.getValue()).append(", ");
        }
        sb.append("]");
        return sb.toString();
    }

    public boolean[] getBucketsList() {
        return buckets;
    }

    public void timer() {
        //System.out.println("[" + simulator.getCurrentTime() + "]\t" + "n"+id + " -> " + "n"+id + "\t Timer");
        onTimer();
    }

    public void onTimer() {
    }

    //Get all buckets this node replicates or all it doesnt (if replicating = false)
    public List<Integer> getReplicatingBuckets(boolean replicating) {
        List<Integer> replicatingBuckets = new ArrayList<>();
        for (int i = 0; i < buckets.length; i++) {
            if (buckets[i] == replicating)
                replicatingBuckets.add(i);
        }
        return replicatingBuckets;
    }

    public int getRandomBucket(boolean replicating) {
        List<Integer> buckets = getReplicatingBuckets(replicating);
        Random randomizer = new Random();
        return buckets.get(randomizer.nextInt(buckets.size()));
    }

    public long getBytesSent() {
        return bytesSent;
    }

    public long getBytesReceived() {
        return bytesReceived;
    }

    public float getAverageBufferSize() {
        return (float) totalBufferSize / nBufferSizes;
    }

    public int getMaxBufferSize() {
        return maxBufferSize;
    }

    public long getAverageDelay(){
        return totalDelay/nDelays;
    }

    public long getMaxDelay() {
        return maxDelay;
    }

    public long getAverageWriteExecutionDelay(){
        return totalWriteExecutionDelay/nWriteExecutionDelays;
    }

    public long getAverageReadExecutionDelay(){
        return totalReadExecutionDelay/nReadExecutionDelays;
    }

    public long getMaxReadExecutionDelay() {
        return maxReadExecutionDelay;
    }

    public long getMaxWriteExecutionDelay() {
        return maxWriteExecutionDelay;
    }
}