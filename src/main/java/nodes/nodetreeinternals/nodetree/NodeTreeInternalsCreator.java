package nodes.nodetreeinternals.nodetree;

import nodes.Node;
import simulator.Simulator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class NodeTreeInternalsCreator {

    public static int nLeafs;

    public static List<Node> createNodes(Simulator sim, String configFile) throws IOException {

        int nNodes;
        int nBuckets;
        int[][] latencyMatrix;
        boolean[][] bucketMatrix;
        int[] nodeSharedLink;
        Map<Integer, List<Integer>>[] treeArray;

        BufferedReader br = new BufferedReader(new FileReader(configFile));

        //latency matrix
        nNodes = Integer.valueOf(br.readLine());
        nLeafs = Integer.valueOf(br.readLine());
        nodeSharedLink = new int[nNodes];
        for(int i = 0;i< nNodes;i++){
            nodeSharedLink[i] = i;
        }

        latencyMatrix = new int[nNodes][nNodes];
        for (int i = 0; i < nNodes; i++) {
            String[] costs = br.readLine().trim().split("\\s+");
            for (int j = 0; j < nNodes; j++) {
                latencyMatrix[i][j] = Integer.valueOf(costs[j]);
            }
            if(costs.length > nNodes){
                nodeSharedLink[i] = Integer.valueOf(costs[nNodes]);
            }
        }
        br.readLine();

        //buckets matrix
        nBuckets = Integer.valueOf(br.readLine());
        bucketMatrix = new boolean[nLeafs][nBuckets];
        for (int i = 0; i < nLeafs; i++) {
            String line = br.readLine();
            String[] buckets = line.trim().split("\\s+");
            for (String bucket : buckets) {
                //System.out.println(buckets[j]);
                bucketMatrix[i][Integer.valueOf(bucket)] = true;
            }
        }
        br.readLine();

        //trees
        treeArray = new Map[nNodes];
        for (int i = nLeafs; i < nNodes; i++) {
            treeArray[i] = new HashMap<>();
            String line;
            while (!(line = br.readLine()).equals(".")) {
                String[] tokens = line.split(":");
                int origin = Integer.valueOf(tokens[0]);
                int dest = Integer.valueOf(tokens[1]);
                treeArray[i].putIfAbsent(origin, new LinkedList<>());
                treeArray[i].get(origin).add(dest);
            }
        }

        //create
        List<Node> nodes = new ArrayList<>();
        for (int i = 0; i < nNodes; i++) {
            Node newNode;
            if(i<nLeafs){
                newNode = new NodeTreeInternalsNode(i, nodes, sim, bucketMatrix[i], treeArray[i], true, nodeSharedLink[i]);
            } else {
                newNode = new NodeTreeInternalsNode(i, nodes, sim, null, treeArray[i], false, nodeSharedLink[i]);
            }
            for (int j = 0; j < nNodes; j++) {
                if (latencyMatrix[i][j] != -1) {
                    newNode.addConnection(j, latencyMatrix[i][j]);
                }
            }
            nodes.add(newNode);
        }

        for (Node n : nodes) {
            NodeTreeInternalsNode ntN = (NodeTreeInternalsNode) n;
            ntN.setup(nLeafs);
        }

        //print
        if (Simulator.LOG_ALG_DEBUG) {
            System.out.println("nNodes: " + nNodes);
            System.out.println("nBuckets: " + nBuckets);
            System.out.println();

            for (int i = 0; i < nNodes; i++) {
                System.out.println("Node " + i);

                System.out.print("\tLatencies: ");
                for (int val : latencyMatrix[i]) {
                    System.out.print(val + " ");
                }
                System.out.println();

                System.out.print("\tBuckets: ");
                for (boolean val : bucketMatrix[i]) {
                    System.out.print((val ? "1" : "0") + " ");
                }
                System.out.println();

                System.out.print("\tTree:");
                Map<Integer, List<Integer>> nodeTree = treeArray[i];
                for (Map.Entry e : nodeTree.entrySet()) {
                    System.out.print(e.getKey() + "->" + e.getValue() + " ");
                }
                System.out.println();
            }
        }
        return nodes;
    }

}
