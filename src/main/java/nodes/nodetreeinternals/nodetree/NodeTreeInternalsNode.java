package nodes.nodetreeinternals.nodetree;

import configuration.SimConfig;
import events.Event;
import nodes.Node;
import operations.Operation;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import simulator.Simulator;

import java.util.*;

public class NodeTreeInternalsNode extends Node {

    private Map<Integer, List<Integer>> tree;
    private Map<Integer, Map<Integer, Triple<Integer, Integer, Integer>>> keys;
    private int[] clock;
    private LinkedList<Operation> waitingOps;
    private boolean leaf;
    private LinkedList<Operation> remoteWritesQueue;
    private int nLeafs;

    NodeTreeInternalsNode(int id, List<Node> nodes, Simulator simulator, boolean[] buckets,
                          Map<Integer, List<Integer>> tree, boolean leaf, int nodeSharedLink) {
        super(id, nodes, simulator, buckets, nodeSharedLink);
        this.leaf = leaf;
        if (leaf) {
            keys = new HashMap<>();
            for (int i = 0; i < buckets.length; i++) {
                if (buckets[i]) keys.put(i, new HashMap<>());
            }
        } else {
            this.tree = tree;
        }
        waitingOps = new LinkedList<>();
        remoteWritesQueue = new LinkedList<>();
    }

    void setup(int nLeafs) {
        this.nLeafs = nLeafs;
        clock = new int[nLeafs];
    }

    @Override
    public long startProcessing(Operation op, int from, Event.EntityType fromType) {
        switch (op.type) {
            case "write":
            case "remote_write":
                return handleWrite(op, from, fromType);
            case "read":
            case "remote_read":
                return handleRead(op, from, fromType);
            case "read_response":
                return handleReadResponse(op, from, fromType);
            case "migrate":
                return handleMigrate(op, from, fromType);
            case "clock":
                return handleClock(op, from, fromType);
            case "data":
                //do nothing
                return BASE_PROCESS_TIME;
            default:
                System.out.println("Unknown message type received: " + op.type);
                System.exit(1);
        }
        return 0;
    }

    private long handleWrite(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;

        //Send data to nodes if from client
        if (fromType == Event.EntityType.CLIENT) {
            assert isLeaf();
            assert (op.type.equals("write") && op.node == id) ||
                    (op.type.equals("remote_write") && op.clientNode == id);
            Operation dataOp = op.deepClone();
            dataOp.type = "data";
            assert dataOp.data != null;

            for (Node n : nodes) {
                if (n == this || !((NodeTreeInternalsNode) n).isLeaf()) continue;
                if (n.getBucketsList()[op.bucket]) {
                    propagateOpToNode(dataOp, n.getId());
                }
            }
            op.data = null;
        }

        //remote write not yet at target
        if (op.clock == null && op.node != id) {
            assert op.type.equals("remote_write");

            //Store to later reply
            if (op.clientNode == id) {
                assert isLeaf();
                Operation queueOp = op.deepClone();
                queueOp.data = null;
                remoteWritesQueue.add(queueOp);
                propagateOpToNode(op, id + nLeafs);
                return time;
            } else {
                assert !isLeaf();
                if (id == op.node + nLeafs) {
                    propagateOpToNode(op, op.node);
                    return time;
                } else {
                    //propagate
                    boolean found = false;
                    for (int nextNode : nextInTree(id, op.clientNode + nLeafs)) {
                        if (leadsTo(nextNode, op.node + nLeafs, op.clientNode + nLeafs)) {
                            propagateOpToNode(op, nextNode);
                            found = true;
                            break;
                        }
                    }
                    assert found;
                    return time;
                }
            }
        }

        //write/remote write arrived at target -----------------------------------
        if (op.node == id) {
            assert fromType != Event.EntityType.CLIENT || op.type.equals("write");
            assert fromType != Event.EntityType.NODE || op.type.equals("remote_write");

            //add clock
            op = op.deepClone();
            op.data = null;
            op.clock = new int[clock.length];
            clock[id] = clock[id] + 1;
            System.arraycopy(clock, 0, op.clock, 0, clock.length);
            propagateOpToNode(op, id + nLeafs);
        }

        //if relevant add to queue, else update clock and check if is effect of remote write
        if (leaf) {
            if (buckets[op.bucket]) {
                waitingOps.addFirst(op);
                time += checkWaitingOps();
            } else {
                updateClock(op.clock);
                time += checkWaitingOps();
                checkRemoteWriteQueue(op);
            }
            return time;
        } else {
            //create clock op
            Operation clockOp = new Operation();
            clockOp.id = op.id;
            clockOp.clientId = op.clientId;
            clockOp.type = "clock";
            clockOp.node = op.node;
            clockOp.clockSingle = op.clock[op.node];

            //propagate
            for (int nextNode : nextInTree(id, op.node + nLeafs)) {
                if (isInterested(nextNode, op.node + nLeafs, op.bucket)) {
                    propagateOpToNode(op, nextNode);
                } else {
                    propagateOpToNode(clockOp, nextNode);
                }
            }
            if (id - nLeafs != op.node)
                propagateOpToNode(op, id - nLeafs);
            return time;
        }

    }

    private long handleReadResponse(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;
        if(leaf){
            assert op.clientNode == id;
            assert op.clock != null;
            waitingOps.addFirst(op);
            time += checkWaitingOps();
            return time;
        } else if (id == (op.clientNode + nLeafs)) {
            propagateOpToNode(op, op.clientNode);
            return time;
        } else {
            for (int nextNode : nextInTree(id, op.node + nLeafs)) {
                if (leadsTo(nextNode, op.clientNode + nLeafs, op.node + nLeafs)) {
                    propagateOpToNode(op, nextNode);
                    return time;
                }
            }
        }
        assert false;
        return 0;
    }

    private long handleRead(Operation op, int from, Event.EntityType fromType) {

        long time = BASE_PROCESS_TIME;
        if (op.node == id) { //respond
            assert buckets[op.bucket];
            Triple<Integer, Integer, Integer> triple = keys.get(op.bucket).get(op.key);
            assert op.bufferArrivalTime != 0;
            long delay = simulator.getCurrentTime() - op.bufferArrivalTime;
            nReadExecutionDelays++;
            totalReadExecutionDelay += delay;
            if (delay > maxReadExecutionDelay)
                maxReadExecutionDelay = delay;

            op = op.deepClone();
            if(op.type.equals("read")){
                assert op.clock==null;
                op.data = (triple != null ? triple.getLeft() : -1); //assume theres always data to read
                op.type = "read_response";
                propagateOpToClient(op, op.clientId);
                return ((SimConfig.dataSize * 1000000) / READ_SPEED) + time;
            } else if(op.type.equals("remote_read")) {
                assert op.clock != null;
                //remote
                waitingOps.addFirst(op);
                time += checkWaitingOps();
                return time;
            } else {
                System.out.println("BUG");
                System.exit(1);
                assert false;
                return 0;
            }

        } else { //propagate
            assert (op.type.equals("remote_read"));
            if (leaf) {
                op.clock = new int[clock.length];
                System.arraycopy(clock, 0, op.clock, 0, clock.length);
                propagateOpToNode(op, id + nLeafs);
                return time;
            } else if (id == (op.node + nLeafs)) {
                assert op.clock != null;
                propagateOpToNode(op, op.node);
                return time;
            } else {
                assert op.clock != null;
                for (int nextNode : nextInTree(id, op.clientNode + nLeafs)) {
                    if (leadsTo(nextNode, op.node + nLeafs, op.clientNode + nLeafs)) {
                        propagateOpToNode(op, nextNode);
                        return time;
                    }
                }
            }
            assert false;
            return 0;
        }
    }

    private long handleMigrate(Operation op, int from, Event.EntityType fromType) {

        long time = BASE_PROCESS_TIME;
        if (op.node == id) {
            assert op.clock == null;
            op = op.deepClone();
            op.clock = new int[clock.length];
            System.arraycopy(clock, 0, op.clock, 0, clock.length);
            propagateOpToNode(op, id + nLeafs);
            return time;
        }

        if (op.target == id) { //Check if I'm target
            waitingOps.addFirst(op);
            return time + checkWaitingOps();
        } else if (id == op.target + nLeafs) {
            propagateOpToNode(op, op.target);
            return time;
        } else {
            for (int nextNode : nextInTree(id, op.node + nLeafs)) {
                if (leadsTo(nextNode, op.target + nLeafs, op.node + nLeafs)) {
                    propagateOpToNode(op, nextNode);
                    return time;
                }
            }
        }
        assert false;
        return 0;
    }

    private long handleClock(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;
        if (leaf) {
            clock[op.node] = Math.max(clock[op.node], op.clockSingle);
            time += checkWaitingOps();
            checkRemoteWriteQueue(op);
        } else {
            for (int nextNode : nextInTree(id, op.node + nLeafs)) {
                propagateOpToNode(op, nextNode);
            }
            propagateOpToNode(op, id - nLeafs);
        }
        return time;
    }

    private void checkRemoteWriteQueue(Operation op) {
        Iterator<Operation> it = remoteWritesQueue.iterator();
        while (it.hasNext()) {
            Operation queueOp = it.next();
            if (op.id.intValue() == queueOp.id.intValue() && op.clientId.intValue() == queueOp.clientId.intValue()) {
                propagateOpToClient(queueOp, queueOp.clientId);
                it.remove();
            }
        }
    }

    private long checkWaitingOps() {
        boolean executed;
        long time = 0;
        do {
            executed = false;
            Iterator<Operation> it = waitingOps.iterator();
            while (it.hasNext()) {
                Operation nextOp = it.next();
                if (canExecute(nextOp)) {
                    switch (nextOp.type) {
                        case "write":
                        case "remote_write":
                            assert nextOp.bufferArrivalTime != 0;
                            long delay = simulator.getCurrentTime() - nextOp.bufferArrivalTime;
                            nWriteExecutionDelays++;
                            totalWriteExecutionDelay += delay;
                            if (delay > maxWriteExecutionDelay)
                                maxWriteExecutionDelay = delay;

                            if (opIsStrictNewer(nextOp.bucket, nextOp.key, nextOp.clock[nextOp.node], nextOp.node))
                                keys.get(nextOp.bucket).put(nextOp.key,
                                        new ImmutableTriple<>(1, nextOp.clock[nextOp.node], nextOp.node));
                            updateClock(nextOp.clock);

                            time += ((SimConfig.dataSize * 1000000) / WRITE_SPEED);

                            simulator.opTimes[nextOp.clientId][nextOp.id][1] =
                                    Math.max(simulator.getCurrentTime() + time, simulator.opTimes[nextOp.clientId][nextOp.id][1]);

                            if (nextOp.node == id && nextOp.type.equals("write")) {
                                Operation clientResponse = nextOp.deepClone();
                                clientResponse.clock = null;
                                propagateOpToClient(clientResponse, clientResponse.clientId);
                            }
                            break;
                        case "migrate":
                            Operation clientResponse = nextOp.deepClone();
                            clientResponse.clock = null;
                            propagateOpToClient(clientResponse, clientResponse.clientId);
                            break;
                        case "remote_read":
                            Triple<Integer, Integer, Integer> triple = keys.get(nextOp.bucket).get(nextOp.key);
                            nextOp = nextOp.deepClone();
                            Operation dataOp = nextOp.deepClone();
                            dataOp.data = (triple != null ? triple.getLeft() : -1); //assume theres always data to read
                            time += ((SimConfig.dataSize * 1000000) / READ_SPEED);
                            dataOp.type = "data";
                            propagateOpToNode(dataOp, nextOp.clientNode);
                            nextOp.type = "read_response";
                            nextOp.clock = new int[clock.length];
                            System.arraycopy(clock, 0, nextOp.clock, 0, clock.length);
                            propagateOpToNode(nextOp, id+nLeafs);
                            break;
                        case "read_response":
                            assert clock != null;
                            nextOp = nextOp.deepClone();
                            nextOp.data = -1;
                            propagateOpToClient(nextOp, nextOp.clientId);
                            break;
                        default:
                            System.out.println("Unknown message type in queue: " + nextOp.type);
                            System.exit(1);
                    }
                    executed = true;
                    it.remove();
                }
            }
        } while (executed);
        return time;
    }

    private void updateClock(int[] opClock) {
        for (int i = 0; i < clock.length; i++) {
            if (opClock[i] > clock[i])
                clock[i] = opClock[i];
        }
    }

    private boolean opIsStrictNewer(int bucket, int key, int opClock, int originalNode) {
        Map<Integer, Triple<Integer, Integer, Integer>> bucketKeys = keys.get(bucket);
        Triple<Integer, Integer, Integer> triple = bucketKeys.get(key);
        return triple == null || triple.getMiddle() < opClock || triple.getMiddle() <= opClock &&
                triple.getRight() < originalNode;
    }

    private boolean canExecute(Operation op) {

        for (int i = 0; i < clock.length; i++) {
            if (i != op.node && op.clock[i] > clock[i])
                return false;
            if (i == op.node && op.clock[i] > clock[i] + 1)
                return false;
        }

        int originalAhead = op.clock[op.node] - clock[op.node];

        if (op.type.equals("migrate")) return originalAhead <= 0;
        else return originalAhead <= 1;
    }

    private boolean isInterested(int node, int originalNode, int bucket) {
        NodeTreeInternalsNode n = (NodeTreeInternalsNode) nodes.get(node - nLeafs);
        if (n.getBucketsList()[bucket]) {
            return true;
        } else {
            for (Integer next : nextInTree(node, originalNode)) {
                if (isInterested(next, originalNode, bucket))
                    return true;
            }
        }
        return false;
    }

    private boolean leadsTo(int node, int targetNode, int originalNode) {
        if (node == targetNode) {
            return true;
        } else {
            for (Integer next : nextInTree(node, originalNode)) {
                if (leadsTo(next, targetNode, originalNode))
                    return true;
            }
        }
        return false;
    }

    private List<Integer> nextInTree(int node, int originalNode) {
        NodeTreeInternalsNode n = (NodeTreeInternalsNode) nodes.get(originalNode);
        Map<Integer, List<Integer>> t = n.tree;
        return t.getOrDefault(node, new LinkedList<>());
    }

    public boolean isLeaf() {
        return leaf;
    }
}