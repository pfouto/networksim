package nodes.buckettree;

import configuration.SimConfig;
import events.Event;
import nodes.Node;
import operations.Operation;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import simulator.Simulator;

import java.util.*;

public class BucketTreeNode extends Node {

    private int[] storageCounters;
    private int[] propagationCounters;
    //[bucket, [key, [value, timestamp, nodeId]]
    private Map<Integer, Map<Integer, Triple<Integer, Integer, Integer>>> keys;
    //[bucket, [key, [depBucket, Triple<depKey, timestamp, nodeId>]]]
    private Map<Integer, Map<Integer, Map<Integer, List<Triple<Integer, Integer, Integer>>>>> deps;

    private LinkedList<Triple<Operation, Integer, Event.EntityType>> waitingOps;

    private int[] bucketInternals;

    boolean leaf;

    BucketTreeNode(int id, List<Node> nodes, Simulator simulator, boolean[] buckets,
                   boolean leaf, int nodeSharedLink, int[] bucketInternals) {
        super(id, nodes, simulator, buckets, nodeSharedLink);
        this.leaf = leaf;
        this.bucketInternals = bucketInternals;
        if(leaf){
            storageCounters = new int[buckets.length];
            propagationCounters = new int[buckets.length];
            keys = new HashMap<>();
            deps = new HashMap<>();
            for (int i = 0; i < buckets.length; i++) {
                if (buckets[i]) {
                    keys.put(i, new HashMap<>());
                    deps.put(i, new HashMap<>());
                }
            }
        }
        waitingOps = new LinkedList<>();
    }

    @Override
    public long startProcessing(Operation op, int from, Event.EntityType fromType) {
        //TODO
        switch (op.type) {
            case "write":
            case "remote_write":
                return handleWrite(op, from, fromType);
            case "read":
            case "remote_read":
                return handleRead(op, from, fromType);
            case "data":
                if(!leaf){
                    System.out.println("Non leaf receiving data, ups " + op);
                    System.exit(1);
                }
                //do nothing
                return BASE_PROCESS_TIME;
            default:
                System.out.println("Unknown message type received: " + op.type);
                System.exit(1);
        }
        return 0;
    }

    private long handleRead(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;
        if(!leaf){
            System.out.println("Internal should never receive reads... " + op);
            System.exit(1);
        }
        //Respond to client if directed to me or propagate if not
        if(op.node == id){
            waitingOps.addFirst(new ImmutableTriple<>(op, from, fromType));
            time += checkWaitingOps();
        } else {
            assert op.type.equals("remote_read");
            propagateOpToNode(op, op.node);
        }
        return time;
    }

    private long handleWrite(Operation op, int from, Event.EntityType fromType) {

        long time = BASE_PROCESS_TIME;
        if(!leaf){
            assert Event.EntityType.NODE == fromType;
            for(Node n: nodes){
                BucketTreeNode bucketTreeNode = (BucketTreeNode) n;
                if(id != n.getId() && n.getId() != from && bucketTreeNode.isLeaf() && n.getBucketsList()[op.bucket]){
                    propagateOpToNode(op, n.getId());
                }
            }
            return time;
        }

        //remote_write - propagate
        if(fromType == Event.EntityType.CLIENT && op.node != id){
            assert op.type.equals("remote_write");
            propagateOpToNode(op, op.node);
            return time;
        }

        //for me - Set version and propagate
        if (op.node == id) {
            Operation dataOp = op.deepClone();
            dataOp.type = "data";
            dataOp.dependencies = null;
            op = op.deepClone();
            propagationCounters[op.bucket]++;
            op.data = null;
            op.version = propagationCounters[op.bucket];

            propagateOpToNode(op, bucketInternals[op.bucket]);

            assert dataOp.data != null;
            for(Node n : nodes){
                if(n == this || !((BucketTreeNode) n).isLeaf()) continue;
                if(n.getBucketsList()[op.bucket]){
                    propagateOpToNode(dataOp, n.getId());
                }
            }
        }

        waitingOps.addFirst(new ImmutableTriple<>(op, from, fromType));
        return time + checkWaitingOps();
    }

    private long checkWaitingOps() {
        boolean executed;
        long time = 0;
        do {
            executed = false;
            Iterator<Triple<Operation, Integer, Event.EntityType>> it = waitingOps.iterator();
            while (it.hasNext()) {
                Triple<Operation, Integer, Event.EntityType> Triple = it.next();
                Operation nextOp = Triple.getLeft();

                if (canExecute(nextOp)) {
                    long delay;
                    switch (nextOp.type) {
                        case "write":
                        case "remote_write":
                            //check or set version
                            storageCounters[nextOp.bucket] = Math.max(storageCounters[nextOp.bucket], nextOp.version);
                            propagationCounters[nextOp.bucket] = Math.max(storageCounters[nextOp.bucket], nextOp.version);

                            assert nextOp.bufferArrivalTime != 0;
                            delay = simulator.getCurrentTime() - nextOp.bufferArrivalTime;
                            nWriteExecutionDelays++;
                            totalWriteExecutionDelay+=delay;
                            if(delay > maxWriteExecutionDelay)
                                maxWriteExecutionDelay = delay;

                            //check if newer to apply
                            Triple<Integer, Integer, Integer> oldVal = keys.get(nextOp.bucket).get(nextOp.key);
                            if (oldVal == null || oldVal.getMiddle() < nextOp.version ||
                                    (oldVal.getMiddle().intValue() == nextOp.version && oldVal.getRight() < nextOp.node)) {
                                //replace value and deps
                                keys.get(nextOp.bucket).put(nextOp.key, new ImmutableTriple<>(1, nextOp.version, nextOp.node));
                                deps.get(nextOp.bucket).put(nextOp.key, nextOp.dependencies);
                            }

                            time += ((SimConfig.dataSize * 1000000L) / WRITE_SPEED);
                            assert simulator.getCurrentTime() >= simulator.opTimes[nextOp.clientId][nextOp.id][1];
                            simulator.opTimes[nextOp.clientId][nextOp.id][1] = simulator.getCurrentTime();
                            //propagate client
                            if (nextOp.node == id) {
                                Operation clientResponse = nextOp.deepClone();
                                clientResponse.data = null;
                                clientResponse.dependencies = null;
                                propagateOpToClient(clientResponse, clientResponse.clientId);
                            }
                            break;
                        case "read":
                        case "remote_read":
                            nextOp.dependencies = null;
                            Operation clientResponse = nextOp.deepClone();
                            clientResponse.response = keys.get(nextOp.bucket).get(nextOp.key);
                            if(clientResponse.response == null)
                                clientResponse.response = new ImmutableTriple<>(-1,null,null);

                            Map<Integer, List<Triple<Integer, Integer, Integer>>> keyDeps = new HashMap<>();
                            if(deps.get(nextOp.bucket).containsKey(nextOp.key)) {
                                for (Map.Entry<Integer, List<Triple<Integer, Integer, Integer>>> e :
                                        deps.get(nextOp.bucket).get(nextOp.key).entrySet()) {
                                    keyDeps.put(e.getKey(), new LinkedList<>());
                                    for (Triple<Integer, Integer, Integer> t : e.getValue()) {
                                        keyDeps.get(e.getKey()).add(new ImmutableTriple<>(t.getLeft(), t.getMiddle(), t.getRight()));
                                    }
                                }
                            }
                            clientResponse.dependencies = keyDeps;

                            time += ((SimConfig.dataSize * 1000000L) / READ_SPEED);
                            propagateOpToClient(clientResponse, clientResponse.clientId);

                            assert nextOp.bufferArrivalTime != 0;
                            delay = simulator.getCurrentTime() - nextOp.bufferArrivalTime;
                            nReadExecutionDelays++;
                            totalReadExecutionDelay+=delay;
                            if(delay > maxReadExecutionDelay)
                                maxReadExecutionDelay = delay;
                            break;
                        default:
                            System.out.println("Unknown message type in queue: " + nextOp.type);
                            System.exit(1);
                    }
                    executed = true;
                    it.remove();
                }
            }
        } while (executed);
        return time;
    }

    private boolean canExecute(Operation op) {
        try{
            int bucket;
            for (Map.Entry<Integer, List<Triple<Integer, Integer, Integer>>> e : op.dependencies.entrySet()) {
                bucket = e.getKey();
                if (!buckets[bucket])
                    continue;

                for (Triple<Integer, Integer, Integer> dep : e.getValue()) {
                    Triple<Integer, Integer, Integer> curr = keys.get(bucket).get(dep.getLeft());
                    if (curr == null || curr.getMiddle() < dep.getMiddle() ||
                            ((int)curr.getMiddle() == dep.getMiddle() && curr.getRight() < dep.getRight()))
                        return false;
                }
            }
            return true;
        } catch (Exception e){
            System.out.println(op);
            throw e;
        }

    }

    public boolean isLeaf() {
        return leaf;
    }
}
