package nodes.buckettree;

import nodes.Node;
import simulator.Simulator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class BucketTreeCreator {

    public static int nLeafs;

    public static List<Node> createNodes(Simulator sim, String configFile) throws IOException {

        int nNodes;
        int nBuckets;
        int[] nodeSharedLink;
        int[][] latencyMatrix;
        boolean[][] bucketMatrix;
        int[] bucketInternal;

        BufferedReader br = new BufferedReader(new FileReader(configFile));

        //latency matrix
        nNodes = Integer.valueOf(br.readLine());
        nLeafs = Integer.valueOf(br.readLine());
        nodeSharedLink = new int[nNodes];
        for(int i = 0;i< nNodes;i++){
            nodeSharedLink[i] = i;
        }

        latencyMatrix = new int[nNodes][nNodes];
        for (int i = 0; i < nNodes; i++) {
            String[] costs = br.readLine().trim().split("\\s+");
            for (int j = 0; j < nNodes; j++) {
                latencyMatrix[i][j] = Integer.valueOf(costs[j]);
            }
            if(costs.length > nNodes){
                nodeSharedLink[i] = Integer.valueOf(costs[nNodes]);
            }
        }
        br.readLine();

        //buckets matrix
        nBuckets = Integer.valueOf(br.readLine());
        bucketMatrix = new boolean[nLeafs][nBuckets];
        for (int i = 0; i < nLeafs; i++) {
            String line = br.readLine();
            String[] buckets = line.trim().split("\\s+");
            for (String bucket : buckets) {
                //System.out.println(buckets[j]);
                bucketMatrix[i][Integer.valueOf(bucket)] = true;
            }
        }
        br.readLine();

        bucketInternal = new int[nBuckets];

        //create
        List<Node> nodes = new ArrayList<>();
        for (int i = 0; i < nNodes; i++) {
            Node newNode;
            if(i < nLeafs){
                newNode = new BucketTreeNode(i, nodes, sim, bucketMatrix[i], true, nodeSharedLink[i],bucketInternal);
            } else {
                newNode = new BucketTreeNode(i, nodes, sim, null, false, nodeSharedLink[i],bucketInternal);
            }
            for (int j = 0; j < nNodes; j++) {
                if (latencyMatrix[i][j] != -1) {
                    newNode.addConnection(j, latencyMatrix[i][j]);
                }
            }
            nodes.add(newNode);
        }


        String tokens[];
        for(int i = 0;i<nBuckets; i++){
            tokens = br.readLine().split(":");
            bucketInternal[Integer.valueOf(tokens[0])] = Integer.valueOf(tokens[1]);
        }

        //print
        if (Simulator.LOG_ALG_DEBUG) {
            System.out.println("nNodes: " + nNodes);
            System.out.println("nBuckets: " + nBuckets);
            System.out.println();

            for (int i = 0; i < nNodes; i++) {
                System.out.println("Node " + i);

                System.out.print("\tLatencies: ");
                for (int val : latencyMatrix[i]) {
                    System.out.print(val + " ");
                }
                System.out.println();

                System.out.print("\tBuckets: ");
                for (boolean val : bucketMatrix[i]) {
                    System.out.print((val ? "1" : "0") + " ");
                }
                System.out.println();
                for (int j = 0; j < nBuckets; j++) {
                    System.out.println("\t\t" + bucketInternal[j]);
                }
            }
        }
        return nodes;
    }
}
