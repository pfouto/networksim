package nodes.nodetree;

import configuration.SimConfig;
import events.Event;
import nodes.Node;
import operations.Operation;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import simulator.Simulator;

import java.util.*;

public class NodeTreeNode extends Node {

    private Map<Integer, List<Integer>> tree;
    private Map<Integer, Map<Integer, Triple<Integer, Integer, Integer>>> keys;
    private int[] clock;
    private LinkedList<Operation> waitingOps;

    private LinkedList<Operation> remoteWritesQueue;

    NodeTreeNode(int id, List<Node> nodes, Simulator simulator,
                 boolean[] buckets, Map<Integer, List<Integer>> tree) {
        super(id, nodes, simulator, buckets);
        this.tree = tree;
        keys = new HashMap<>();
        for (int i = 0; i < buckets.length; i++) {
            if (buckets[i]) keys.put(i, new HashMap<>());
        }
        waitingOps = new LinkedList<>();
        remoteWritesQueue = new LinkedList<>();
    }

    void setup() {
        clock = new int[nodes.size()];
    }

    @Override
    public long startProcessing(Operation op, int from, Event.EntityType fromType) {
        switch (op.type) {
            case "write":
            case "remote_write":
                return handleWrite(op, from, fromType);
            case "read":
            case "remote_read":
                return handleRead(op, from, fromType);
            case "migrate":
                return handleMigrate(op, from, fromType);
            case "clock":
                return handleClock(op, from, fromType);
            case "data":
                //do nothing
                return BASE_PROCESS_TIME;
            default:
                System.out.println("Unknown message type received: " + op.type);
                System.exit(1);
        }
        return 0;
    }

    private long handleWrite(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;

        //Send data to nodes if from client
        if (fromType == Event.EntityType.CLIENT) {
            Operation dataOp = op.deepClone();
            dataOp.type = "data";
            assert dataOp.data != null;
            for (Node n : nodes) {
                if (n == this) continue;
                if (n.getBucketsList()[op.bucket]) {
                    propagateOpToNode(dataOp, n.getId());
                }
            }
            op.data = null;
        }

        //remote write not yet at target
        if (op.clock == null && op.node != id) {
            assert op.type.equals("remote_write");

            //Store to later reply
            if (op.clientNode == id) {
                Operation queueOp = op.deepClone();
                queueOp.data = null;
                remoteWritesQueue.add(queueOp);
            }

            //propagate
            boolean found = false;
            for (int nextNode : nextInTree(id, op.clientNode)) {
                if (leadsTo(nextNode, op.node, op.clientNode)) {
                    propagateOpToNode(op, nextNode);
                    found = true;
                    break;
                }
            }
            assert found;
            return time;
        }

        //write/remote write arrived at target
        if (op.node == id) {
            assert fromType != Event.EntityType.CLIENT || op.type.equals("write");
            assert fromType != Event.EntityType.NODE || op.type.equals("remote_write");

            //add clock
            op = op.deepClone();
            op.data = null;
            op.clock = new int[clock.length];
            clock[id] = clock[id] + 1;
            System.arraycopy(clock, 0, op.clock, 0, clock.length);
        }

        //if relevant add to queue, else update clock and check if is effect of remote write
        if (buckets[op.bucket]) {
            waitingOps.addFirst(op);
            time += checkWaitingOps();
        } else {
            updateClock(op.clock);
            time += checkWaitingOps();
            checkRemoteWriteQueue(op);
        }

        //create clock op
        Operation clockOp = new Operation();
        clockOp.id = op.id;
        clockOp.clientId = op.clientId;
        clockOp.type = "clock";
        clockOp.node = op.node;
        clockOp.clockSingle = op.clock[op.node];

        //propagate
        for (int nextNode : nextInTree(id, op.node)) {
            if (isInterested(nextNode, op.node, op.bucket)) {
                propagateOpToNode(op, nextNode);
            } else {
                propagateOpToNode(clockOp, nextNode);
            }
        }
        return time;
    }

    private long handleRead(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;
        if (op.node == id) { //respond
            assert buckets[op.bucket];
            Triple<Integer, Integer, Integer> triple = keys.get(op.bucket).get(op.key);
            assert op.bufferArrivalTime != 0;
            long delay = simulator.getCurrentTime() - op.bufferArrivalTime;
            nReadExecutionDelays++;
            totalReadExecutionDelay += delay;
            if (delay > maxReadExecutionDelay)
                maxReadExecutionDelay = delay;
            op = op.deepClone();
            op.data = (triple != null ? triple.getLeft() : -1); //assume theres always data to read
            propagateOpToClient(op, op.clientId);
            return ((SimConfig.dataSize * 1000000) / READ_SPEED) + time;

        } else { //propagate
            assert (op.type.equals("remote_read"));
            for (int nextNode : nextInTree(id, op.clientNode)) {
                if (leadsTo(nextNode, op.node, op.clientNode)) {
                    propagateOpToNode(op, nextNode);
                    return time;
                }
            }
            assert false;
            return 0;
        }
    }

    private long handleMigrate(Operation op, int from, Event.EntityType fromType) {

        long time = BASE_PROCESS_TIME;
        if (op.node == id) {
            assert op.clock == null;
            op = op.deepClone();
            op.clock = new int[clock.length];
            System.arraycopy(clock, 0, op.clock, 0, clock.length);
        }

        if (op.target == id) { //Check if I'm target
            waitingOps.addFirst(op);
            time += checkWaitingOps();
        } else {
            boolean found = false;
            for (int nextNode : nextInTree(id, op.node)) {
                if (leadsTo(nextNode, op.target, op.node)) {
                    propagateOpToNode(op, nextNode);
                    found = true;
                    break;
                }
            }
            assert found;
        }
        return time;
    }

    private long handleClock(Operation op, int from, Event.EntityType fromType) {
        long time = BASE_PROCESS_TIME;
        clock[op.node] = Math.max(clock[op.node], op.clockSingle);
        time += checkWaitingOps();
        checkRemoteWriteQueue(op);
        for (int nextNode : nextInTree(id, op.node)) {
            propagateOpToNode(op, nextNode);
        }
        return time;
    }

    private void checkRemoteWriteQueue(Operation op) {
        Iterator<Operation> it = remoteWritesQueue.iterator();
        while (it.hasNext()) {
            Operation queueOp = it.next();
            if (op.id.intValue() == queueOp.id.intValue() && op.clientId.intValue() == queueOp.clientId.intValue()) {
                propagateOpToClient(queueOp, queueOp.clientId);
                it.remove();
            }
        }
    }

    private long checkWaitingOps() {
        boolean executed;
        long time = 0;
        do {
            executed = false;
            Iterator<Operation> it = waitingOps.iterator();
            while (it.hasNext()) {
                Operation nextOp = it.next();
                if (canExecute(nextOp)) {
                    switch (nextOp.type) {
                        case "write":
                        case "remote_write":
                            assert nextOp.bufferArrivalTime != 0;
                            long delay = simulator.getCurrentTime() - nextOp.bufferArrivalTime;
                            nWriteExecutionDelays++;
                            totalWriteExecutionDelay += delay;
                            if (delay > maxWriteExecutionDelay)
                                maxWriteExecutionDelay = delay;

                            if (opIsStrictNewer(nextOp.bucket, nextOp.key, nextOp.clock[nextOp.node], nextOp.node))
                                keys.get(nextOp.bucket).put(nextOp.key,
                                        new ImmutableTriple<>(1, nextOp.clock[nextOp.node], nextOp.node));
                            updateClock(nextOp.clock);

                            time += ((SimConfig.dataSize * 1000000) / WRITE_SPEED);
                            simulator.opTimes[nextOp.clientId][nextOp.id][1] = simulator.getCurrentTime();

                            if (nextOp.node == id && nextOp.type.equals("write")) {
                                Operation clientResponse = nextOp.deepClone();
                                clientResponse.clock = null;
                                propagateOpToClient(clientResponse, clientResponse.clientId);
                            }
                            break;
                        case "migrate":
                            Operation clientResponse = nextOp.deepClone();
                            clientResponse.clock = null;
                            propagateOpToClient(clientResponse, clientResponse.clientId);
                            break;
                        default:
                            System.out.println("Unknown message type in queue: " + nextOp.type);
                            System.exit(1);
                    }
                    executed = true;
                    it.remove();
                }
            }
        } while (executed);
        return time;
    }

    private void updateClock(int[] opClock) {
        for (int i = 0; i < clock.length; i++) {
            if (opClock[i] > clock[i])
                clock[i] = opClock[i];
        }
    }

    private boolean opIsStrictNewer(int bucket, int key, int opClock, int originalNode) {
        Map<Integer, Triple<Integer, Integer, Integer>> bucketKeys = keys.get(bucket);
        Triple<Integer, Integer, Integer> triple = bucketKeys.get(key);
        return triple == null || triple.getMiddle() < opClock || triple.getMiddle() <= opClock &&
                triple.getRight() < originalNode;
    }

    private boolean canExecute(Operation op) {

        for (int i = 0; i < clock.length; i++) {
            if (i != op.node && op.clock[i] > clock[i])
                return false;
            if (i == op.node && op.clock[i] > clock[i] + 1)
                return false;
        }

        int originalAhead = op.clock[op.node] - clock[op.node];

        if (op.type.equals("migrate")) return originalAhead <= 0;
        else return originalAhead <= 1;
    }

    private boolean isInterested(int node, int originalNode, int bucket) {
        NodeTreeNode n = (NodeTreeNode) nodes.get(node);
        if (n.getBucketsList()[bucket]) {
            return true;
        } else {
            for (Integer next : nextInTree(node, originalNode)) {
                if (isInterested(next, originalNode, bucket))
                    return true;
            }
        }
        return false;
    }

    private boolean leadsTo(int node, int targetNode, int originalNode) {
        if (node == targetNode) {
            return true;
        } else {
            for (Integer next : nextInTree(node, originalNode)) {
                if (leadsTo(next, targetNode, originalNode))
                    return true;
            }
        }
        return false;
    }

    private List<Integer> nextInTree(int node, int originalNode) {
        return ((NodeTreeNode) nodes.get(originalNode)).tree.getOrDefault(node, new LinkedList<>());
    }

}