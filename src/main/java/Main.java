import configuration.SimConfig;
import plot.PlotDataCreator;
import simulator.Simulator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws IOException {

        Properties prop = new Properties();
        InputStream input = new FileInputStream("res/simulator/config");
        prop.load(input);

        String[] dataSizeSet = prop.getProperty("dataSizeSet").split(",");
        String[] migrateSet = prop.getProperty("migrateSet").split(",");
        String [] nodeTypeSet = prop.getProperty("nodeTypeSet").split(",");
        String[] nodeConfigFileSet = prop.getProperty("nodeConfigFileSet").split(",");
        String[] nodeConfigNameSet = prop.getProperty("nodeConfigNameSet").split(",");
        assert nodeConfigFileSet.length == nodeTypeSet.length;
        input.close();

        SimConfig.LoadConfig("res/simulator/config");

        for(String dataSize: dataSizeSet){
            SimConfig.dataSize = Integer.valueOf(dataSize);
            System.out.println("DataSize: " + dataSize);
            for(String migrate: migrateSet){
                SimConfig.migrate = Boolean.valueOf(migrate);
                System.out.println("\tMigrate: " + migrate);
                for(int i = 0;i<nodeTypeSet.length;i++){
                    SimConfig.nodeType = nodeTypeSet[i];
                    SimConfig.nodeConfigFile = nodeConfigFileSet[i];
                    SimConfig.configName = nodeConfigNameSet[i];
                    System.out.println("\t\t" + SimConfig.nodeType + " " +
                            SimConfig.nodeConfigFile.substring(SimConfig.nodeConfigFile.lastIndexOf("/") + 1));
                    new Simulator().startSimulation();
                }
            }
        }

        PlotDataCreator.main(null);

    }
}