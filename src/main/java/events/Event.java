package events;

import operations.Operation;

public class Event {

    public enum EventType {
        PROPAGATION, PROCESSING, TIMER
    }

    public enum EntityType {
        NODE, CLIENT
    }

    private EventType type;
    private Operation op;
    private int target;
    private int from;
    private EntityType targetType;
    private EntityType fromType;

    private long time;


    public Event(Operation op, EventType type, int target, EntityType targetType, int from, EntityType fromType, long time){
        this.op = op;
        this.target = target;
        this.from = from;
        this.type = type;
        this.time = time;
        this.fromType = fromType;
        this.targetType = targetType;
    }

    public EventType getType() {
        return type;
    }

    public int getTarget() {
        return target;
    }

    public Operation getOp() {
        return op;
    }

    public long getTime() {
        return time;
    }

    public int getFrom() {
        return from;
    }

    public EntityType getFromType() {
        return fromType;
    }

    public EntityType getTargetType() {
        return targetType;
    }
}
