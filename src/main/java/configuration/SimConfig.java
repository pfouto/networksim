package configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SimConfig {

    public static int nClients;
    public static int nPrepOps;
    public static int nOps;
    public static float readW;
    public static float writeW;
    public static int nKeys;
    public static int maxLocal;
    public static int minLocal;
    public static int maxRemote;
    public static int minRemote;
    public static String distribution;
    public static int localPoisson;
    public static int remotePoisson;
    public static String nodeType;
    public static String nodeConfigFile;
    public static boolean migrate;
    public static long dataSize;
    public static String configName;

    public static void printConfig(){
        System.out.println("Clients: " + nClients);
        System.out.println("Ops: " + nPrepOps + " + " + nOps);
        System.out.println("Weight Read " + readW + ", Write " + writeW);
        System.out.println("nKeys: " + nKeys);
        System.out.println("Distribution: " + distribution);
        if(distribution.equals("poisson")){
            System.out.println("local " + localPoisson + ", remote " + remotePoisson);
        } else if(distribution.equals("uniform")){
            System.out.println("minLocal " + minLocal + " maxLocal " + maxLocal + " minRemote" + minRemote + " maxRemote" + maxRemote);
        }
        System.out.println("Migrate: " + migrate);
        System.out.println("DataSize: " + dataSize);
        System.out.println("Node Type: " + nodeType);
        System.out.println("Config File: " + nodeConfigFile);
    }

    public static void LoadConfig(String configFile) throws IOException {
        Properties prop = new Properties();
        InputStream input = new FileInputStream(configFile);
        prop.load(input);

        nPrepOps = Integer.valueOf(prop.getProperty("nPrepOps"));
        nOps = Integer.valueOf(prop.getProperty("nOps"));
        nClients = Integer.valueOf(prop.getProperty("nClients"));
        readW = Float.valueOf(prop.getProperty("readW"));
        writeW = Float.valueOf(prop.getProperty("writeW"));
        nKeys = Integer.valueOf(prop.getProperty("nKeys"));
        minLocal = Integer.valueOf(prop.getProperty("minLocal"));
        maxLocal = Integer.valueOf(prop.getProperty("maxLocal"));
        minRemote = Integer.valueOf(prop.getProperty("minRemote"));
        maxRemote = Integer.valueOf(prop.getProperty("maxRemote"));
        localPoisson = Integer.valueOf(prop.getProperty("localPoisson"));
        remotePoisson = Integer.valueOf(prop.getProperty("remotePoisson"));
        distribution = prop.getProperty("distribution");
        input.close();
    }
}
