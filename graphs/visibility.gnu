set terminal pngcairo  transparent enhanced font "arial,14" fontscale 1.0 size 1000, 600
set output 'visibility.png'
set boxwidth 0.9 absolute
set style fill   solid 1.00 border lt -1
set key out
set style histogram clustered gap 1 title textcolor lt -1
set datafile missing '-'
set style data histograms
set xtics border in scale 0,0 nomirror rotate by 0  autojustify
set xtics  norangelimit
set xtics   ()
set ylabel "Visibility time (ms)"

set yran [0:]
#set yrange [ 0.00000 : 300000. ] noreverse nowriteback
DEBUG_TERM_HTIC = 119
DEBUG_TERM_VTIC = 119
plot 'visibility.dat' using 2:xtic(1) ti col, for [i=3:5] '' u i ti col
