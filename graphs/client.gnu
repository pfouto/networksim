set terminal pngcairo  transparent enhanced font "arial,20" fontscale 1.0 size 1000, 600
set boxwidth 0.9 absolute
set style fill   solid 1.00 border lt -1
set key out
set style histogram clustered gap 1 title textcolor lt -1
set datafile missing '-'
set style data histograms
set xtics border in scale 0,0 nomirror autojustify
set xtics  norangelimit
set xtics   ()
set yran [0:]
set ylabel "Latency (ms)"
#set yrange [ 0.00000 : 300000. ] noreverse nowriteback
DEBUG_TERM_HTIC = 119
DEBUG_TERM_VTIC = 119
## Last datafile plotted: "immigration.dat"
set output 'client_overall.png'
plot for [i=2:17:4] 'client.dat' using i:xtic(1) t col
#set yran [0:300]
#set output 'client_remote.png'
plot for [i=4:17:4] 'client.dat' u i:xtic(1) t col
#set output 'client_migrate.png'
#plot for [i=5:17:4] 'client_migrate.dat' u i:xtic(1) t col

