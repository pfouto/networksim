set terminal pngcairo  transparent enhanced font "arial,14" fontscale 1.0 size 1000, 600
set output 'bytessent.png'
set boxwidth 0.9 absolute
set style fill   solid 1.00 border lt -1
set key out right top vertical Right noreverse noenhanced autotitle nobox
set style histogram clustered gap 1 title textcolor lt -1
set datafile missing '-'
set style data histograms
set xtics border in scale 0,0 nomirror  autojustify
set xtics  norangelimit
set xtics   ()

set xlabel "1                               100                             1000"
set ylabel "Number of Bytes Sent"
#set yrange [ 0.00000 : 300000. ] noreverse nowriteback
DEBUG_TERM_HTIC = 119
DEBUG_TERM_VTIC = 119
## Last datafile plotted: "immigration.dat"
plot 'bytes.dat' using 2:xtic(1) ti col, '' u 4 ti col, '' u 6 ti col, '' u 8 ti col, '' u 10 ti col
